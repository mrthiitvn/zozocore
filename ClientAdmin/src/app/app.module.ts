import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from "ngx-bootstrap";


import { routing } from './app.routing';
//import component
import { ViewRouteComponent } from './components/share/view.route.compnent';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UserComponent } from './components/user/user.component';
import { UserDetailComponent } from './components/user/user.detail.component';
import { OrderComponent } from './components/order/order.component';
import { HeaderComponent } from './components/share/header/header.component';
import { FooterComponent } from './components/share/footer/footer.component';
import { SidebarComponent } from './components/share/sidebar/sidebar.component';
import { ShopComponent } from './components/shop/shop.component';
import { LoginComponent } from './components/login/login.component';
//import service
import { ApiService } from './services/api.service';
import { AuthenticateService } from './services/authenticate.service';
import { LoginService } from './services/login.service';
import { SocketService } from './services/socket.service';
import { UserService } from './services/user.service';

@NgModule({
  declarations: [
    AppComponent,
    ViewRouteComponent,
    DashboardComponent,
    UserComponent,
    UserDetailComponent,
    OrderComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    ShopComponent,
    LoginComponent
    
  ],
  imports: [
    BrowserModule, RouterModule, routing, FormsModule, ReactiveFormsModule, ModalModule.forRoot(),
    HttpModule
  ],
  providers: [ApiService, AuthenticateService, LoginService, SocketService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
