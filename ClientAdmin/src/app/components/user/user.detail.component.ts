import { Component, ViewChild, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'detail-user',
  templateUrl: './user.detail.component.html'
})
export class UserDetailComponent implements OnInit {
  public title: string = "Cao Hoang Chuyen";
  public totalItem: number;
  public modelUser: any = {
    // userId: null,
    firstName: '',
    lastName: '',
    email: '',
    phone: ''
  }
  public seletedUser: any;
  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {debugger;
    this.GetUser();
  }

  GetUser(): void {
    // var modelUser ={userId: 1, firstName: 'Cao', lastName: 'Chuyen', email: 'hoangchuyen@gmail.com', phone: '0945181499'};
    // this.modelUser = modelUser;
  }
  Save(): void {
    debugger;
    this.userService.CreateUser(this.modelUser).then(result => {
      console.log("create user", result);
    });
  }
}
