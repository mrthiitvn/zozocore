import { Component, ViewChild, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  public title: string = "Cao Hoang Chuyen";
  public userList: any;
  public totalItem: number;
  public user: any = {
    userId: null,
    firstName: '',
    lastName: '',
    email: '',
    phone: ''
  }
  public seletedUser: any;
  @ViewChild('deleteUserModal') public deleteUserModal: ModalDirective;
  constructor() { }

  ngOnInit() {
    this.GetUsers();
  }

  GetUsers(): void {
    var modelUsers = [
      {userId: 1, firstName: 'Cao', lastName: 'Chuyen', email: 'hoangchuyen@gmail.com', phone: '0945181499'},
      {userId: 2, firstName: 'Phan', lastName: 'Cuong', email: 'phancuong@gmail.com', phone: '0987654456'}
    ];
    this.userList = modelUsers;
    this.totalItem = modelUsers.length;
  }

  OpenDeleteModal(): void {
    this.deleteUserModal.show();
  }

}
