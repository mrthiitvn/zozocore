import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionData } from '../../model/session.data.model';
import { LoginService } from '../../services/login.service';
import { AuthenticateService } from '../../services/authenticate.service';
import { SocketService } from '../../services/socket.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public session: SessionData;
  public modelLogin: any = {
    userName: '',
    passWord: ''
  }
  public userName = '';
  public passWord = '';
  public socket: any;
  constructor(
    private loginService: LoginService,
    private authenticateService: AuthenticateService,
    private socketService: SocketService,
    private router: Router
  ) {
    this.socketService.socket.subscribe(o => {
      this.session = o;
    });
  }

  ngOnInit() {
    this.session = null;
  }

  Login(): void {debugger;
    this.userName = this.modelLogin.userName;
    this.passWord = this.modelLogin.passWord;
    this.loginService.login(this.userName, this.passWord).then(result => {
      if (result != null && result.success) {
        // var sessionData = this.authenticateService.buildSessionData(result);
        var sessionData = new SessionData(1, 'chuyen@gmail.com', 'Cao', 'Chuyen',
          'abc', true, null, false);
        this.authenticateService.setSessionData(sessionData);
        console.log(sessionData);
        if (this.socket) {
          this.socket.emit('authenticate', { token: sessionData.token });
        }
        console.log('loggged success',);
        this.router.navigate(['/dashboard']);
      } else {
        console.log("login error");
      }
    }).catch(function(error){
      console.log(error);
    });
  }
}
