export class SessionData {
    public userId: number;
    public email: string;
    public token: string;
    public firstName: string;
    public lastName: string;
    public isAdmin: boolean;
    public isUser: boolean;
    public avatar: string;
    constructor(userId: number, email: string, firstName: string, lastName: string, token: string, isAdmin: boolean, avatar: string, isUser: boolean) {
        this.userId = userId;
        this.email = email;
        this.token = token;
        this.firstName = firstName;
        this.lastName = lastName;
        this.isAdmin = isAdmin;
        this.avatar = avatar;
        this.isUser = isUser;
    }

    updateToken(newToken: string) {
        this.token = newToken;
    }
}
