export class UserModel {
    constructor() { }
    public id: number;
    public firstName: String;
    public lastName: String;
    public email: string;
    public password: String;
    public phoneNumber: String;
    public recaptcha: String;
    public emailConfirm: String;
    public passwordConfirm: String;
    public avatar: any;
    public facebookId: number;
    public linkedinId: number;
    public roleId: number;
    public createdAt: Date;
    public selectedRoleId:number;
}