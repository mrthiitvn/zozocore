import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewRouteComponent } from './components/share/view.route.compnent'
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UserComponent } from './components/user/user.component';
import { UserDetailComponent } from './components/user/user.detail.component';
import { OrderComponent } from './components/order/order.component';
import { ShopComponent } from './components/shop/shop.component';
import { LoginComponent } from './components/login/login.component';
const frontPageRoutes: Routes = [
]

const adminRoutes: Routes = [
    // { path: '', component: DashboardComponent},
    // { path: 'user', component: UserComponent},
    // { path: 'user/edit/:id', component: UserDetailComponent},
    // { path: 'order', component: OrderComponent},
    // { path: 'shop', component: ShopComponent}
]

const appRoutes: Routes = [
    { path: '', redirectTo: '/', pathMatch: 'full' },
    // { path: '', component: DashboardComponent, children: adminRoutes},
    { path: 'login', component: LoginComponent},
    { path: 'dashboard', component: DashboardComponent},
    { path: 'user', component: UserComponent},
    { path: 'user/edit/:id', component: UserDetailComponent},
    { path: 'shop', component: ShopComponent},
    { path: 'order', component: OrderComponent}

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);

