import { Component, OnInit} from '@angular/core';
import { Router} from '@angular/router';
import { SessionData } from './model/session.data.model';
import { AuthenticateService } from './services/authenticate.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  public session: SessionData;
  constructor(
    private authenticateService: AuthenticateService,
    private router: Router
  ) { 
    this.authenticateService.getSessionData().subscribe(o => {
      this.session = o;
    });
  }

  ngOnInit() {
    if(this.session == null){
      this.router.navigate(['/login']);
    }
  }
}
