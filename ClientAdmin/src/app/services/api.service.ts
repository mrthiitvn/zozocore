import { ActivatedRoute, Router } from '@angular/router';
import { Headers, RequestOptions, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ApiResult } from '../model/api.result.model';
import { SessionData } from '../model/session.data.model';
import { AuthenticateService } from '../services/authenticate.service';
import 'rxjs/add/operator/toPromise';
@Injectable()
export class ApiService {
    private sessionData: SessionData;

    constructor(private _http: Http, private _authenticateService: AuthenticateService,
        private route: ActivatedRoute,
        private router: Router) {
        this._authenticateService.getSessionData().subscribe(o => { this.sessionData = o });
    }

    httpGet(url: string, addToken: boolean = true): Promise<ApiResult> {
        return this._http.get(url, this.createHeaderToken(addToken)).toPromise().then((response: any) => {
            return response.json();
        }).catch((reason: any) => {
            if (reason.status == 403) {
                this._authenticateService.clearSessionData();
                this.router.navigate(['/login']);
            }
            return Promise.reject(reason as ApiResult);
        });
    }
    httpPost(url: string, body: any, addToken: boolean = true): Promise<ApiResult> {
        return this._http.post(url, JSON.stringify(body), this.createHeaderToken(addToken)).toPromise().then((response: any) => {
            let result = response.json() as ApiResult;
            return result;
        }).catch((reason: any) => {
            if (reason.status == 403) {
                this._authenticateService.clearSessionData();
                this.router.navigate(['/login']);
            }
            var result = {
                success: true,
                data: {
                    userName: 'chuyen',
                    email: 'chuyen@gmail.com'
                },
                message: 'login error'
            }
            return result;
            // return Promise.reject(reason as ApiResult);
        });
    }
    httpPut(url, model, addToken: boolean = true): Promise<ApiResult> {
        return this._http.put(url, JSON.stringify(model), this.createHeaderToken(addToken)).toPromise().then((response: any) => {
            return response.json() as ApiResult;
        }).catch((reason: any) => {
            if (reason.status == 403) {
                this._authenticateService.clearSessionData();
                this.router.navigate(['/login']);
            }
            return Promise.reject(reason as ApiResult);
        });
    }
    httpDelete(url: string, addToken: boolean = true): Promise<ApiResult> {
        return this._http.delete(url, this.createHeaderToken(addToken)).toPromise().then((response: any) => {
            return response.json() as ApiResult;
        }).catch((reason: any) => {
            if (reason.status == 403) {
                this._authenticateService.clearSessionData();
                this.router.navigate(['/login']);
            }
            return Promise.reject(reason as ApiResult);
        });
    }

    createHeaderToken(addToken: boolean): RequestOptions {
        var headers = new Headers();
        headers.append('accept', 'application/json');
        headers.append('Content-Type', 'application/json');

        if (addToken && this.sessionData != null && this.sessionData.token != null) {
            headers.append('x-access-token', this.sessionData.token);
        }
        return new RequestOptions({ headers: headers });
    }
}