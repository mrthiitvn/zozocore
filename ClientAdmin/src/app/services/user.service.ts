import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ApiService} from '../services/api.service';
import {UserModel} from '../model/user.model';
import { ApiResult } from '../model/api.result.model';
@Injectable()
export class UserService {
    constructor(private _apiService: ApiService) {

    }

    CreateUser(user: UserModel): Promise<ApiResult> {
        const url = `${environment.baseApiUrl}/Accounts/CreateUser`;
        return this._apiService.httpPost(url, user, false);
    }
}