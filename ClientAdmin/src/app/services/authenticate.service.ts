import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SessionData } from '../model/session.data.model';
@Injectable()
export class AuthenticateService {
    private sessionObject = new BehaviorSubject<SessionData>(null);
    public returnUrl: string;
    constructor() {
        var token = localStorage.getItem('token');
        if (token != null) {
            var userId = +localStorage.getItem('userId'); //put + to convert string to number
            var firstName = localStorage.getItem('firstName');
            var lastName = localStorage.getItem('lastName');
            var email = localStorage.getItem('email');
            var isAdmin = this.convertStringToBoolean(localStorage.getItem('isAdmin'));
            var isUser = this.convertStringToBoolean(localStorage.getItem('isUser'));
            var avatar = localStorage.getItem('avatar');
            var sessionData = new SessionData(userId, email, firstName, lastName, token, isAdmin, avatar, isUser);
            this.setSessionData(sessionData);
        }
    }
    setSessionData(val: SessionData): void {
        localStorage.setItem('session', JSON.stringify(val));
        this.sessionObject.next(val);
    }
    getSessionData() {
        return this.sessionObject;
    }
    clearSessionData() {
        localStorage.clear();
        this.sessionObject.next(null);
    }

    convertStringToBoolean(val: string): boolean {
        if (val == "true" || val == "yes")
            return true;
        else
            return false;
    }
    updateAvatar(avatar: string) {
        var session = this.getSessionData().value;
        session.avatar = avatar;
        this.setSessionData(session);
    }

    buildSessionData(authResult: any): SessionData {
        var user = authResult.data.user;
        var isAdmin = false;
        var isUser = false;
        user.roles.forEach(function (item, idx) {
            if (item == "Admin") {
                isAdmin = true;
            }
            if (item == "User") {
                isUser = true;
            }
        });
        var sessionData = new SessionData(user.userId, user.email, user.firstName, user.lastName,
            authResult.data.auth_token, isAdmin, user.avatar, isUser);
        return sessionData;
    }

    updateSessionData(key: string, value: any) {
        var session = this.getSessionData().value;
        session[key] = value;
        this.setSessionData(session);
    }
}