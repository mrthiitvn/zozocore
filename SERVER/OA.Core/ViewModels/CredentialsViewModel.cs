﻿using FluentValidation.Attributes;
using OA.Core.ViewModels.Validations;

namespace OA.Core.ViewModels
{
	[Validator(typeof(CredentialsViewModelValidator))]
	public class CredentialsViewModel
	{
		public string UserName { get; set; }
		public string Password { get; set; }
	}
}
