﻿using OA.Core.ViewModels;

namespace OA.Core.Models
{
	public class CustomerEntity
    {
		public int Id { get; set; }
		public string IdentityId { get; set; }
		public RegistrationViewModel Identity { get; set; }  // navigation property
		public string Location { get; set; }
		public string Locale { get; set; }
		public string Gender { get; set; }
	}
}
