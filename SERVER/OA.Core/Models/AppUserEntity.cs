﻿namespace OA.Core.Models
{
	public class AppUserEntity
	{
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Location { get; set; }
        public long? FacebookId { get; set; }
        public string PictureUrl { get; set; }
    }
}
