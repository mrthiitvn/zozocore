﻿using OA.Core.Models;
using System.Security.Claims;
using System.Threading.Tasks;

namespace OA.Core.Repoes
{
    public interface IUserManagerRepo
    {
		//Task<bool> CreateIdentity(RegistrationViewModel registrationViewModel);

        Task<bool> CreateIdentity(AppUserEntity appUserEntity);

        Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password);

		Task<CustomerEntity> GetCustomer(Claim user);

        Task<AppUserEntity> FindUserByEmail(string email);

        Task<AppUserEntity> FindUserByName(string userName);

        Task<string> GetUserId(string userName);

        Task<bool> UpdateUser(AppUserEntity appUserEntity);

        Task<bool> DeleteUser(AppUserEntity appUserEntity);

        Task<bool> AddRole(string role);

        Task<bool> AssignRoleForUser(string userEmail, string role);
    }
}
