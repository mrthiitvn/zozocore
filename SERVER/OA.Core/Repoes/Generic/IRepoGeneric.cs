﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace OA.Core.Repoes.Generic
{
	public interface IRepoGeneric<T> where T : class
	{
		IEnumerable<T> GetAll();
		IEnumerable<T> Find(Expression<Func<T, bool>> where);
		T GetById(long id);
		Task<T> Insert(T entity);
		Task<bool> Update(T entity);
		Task<bool> Delete(long id);
		Task SaveChanges();
	}
}
