﻿namespace OA.Core.Configuration
{
	public class DbConfiguration
    {
		public string ConnectionStrings { get; set; }
		public string DefaultConnection { get; set; }
	}
}
