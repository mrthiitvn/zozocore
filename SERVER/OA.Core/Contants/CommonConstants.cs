﻿namespace OA.Core.Contants
{
    public static class CommonConstants
    {
		public struct Routes
		{
			//public const string ApiController = "api/[controller]";
			//public const string ApiControllerAction = "api/[controller]/[action]";
			public const string BaseRoute = "api/{version:apiVersion}/[controller]/[action]";
            public const string AddNewRole = "{role}";
            public const string AssignRole = "{userEmail}/{role}";

        }

		public struct Authorize
		{
			public const string ApiUser = "ApiUser";
			public const string Admin = "Admin";
			public const string Mod = "Mod";
		}

        public struct FacebookAuthentication
        {
            public static string AccessTokenResponseUri = "https://graph.facebook.com/oauth/access_token?client_id={0}&client_secret={1}&grant_type=client_credentials";
            public static string AccessTokenValidationResponseUri = "https://graph.facebook.com/debug_token?input_token={0}&access_token={1}";
            public static string UserInfoResponseUri = "https://graph.facebook.com/v2.8/me?fields=id,email,first_name,last_name,name,gender,locale,birthday,picture&access_token={0}";
        }
	}
	
}
