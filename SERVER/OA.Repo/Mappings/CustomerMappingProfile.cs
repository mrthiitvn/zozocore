﻿using AutoMapper;
using OA.Core.Models;
using OA.Core.ViewModels;
using OA.Infrastructure.EF.Models;

namespace OA.Repo.Mappings
{
	public class CustomerMappingProfile : Profile
	{
		public CustomerMappingProfile()
		{
			CreateMap<Customer, CustomerEntity>();//.ForMember(dest => dest.Identity, opts => opts.MapFrom(src => src.Identity));
			CreateMap<AppUser, RegistrationViewModel>();//.ForMember(au => au.UserName, map => map.MapFrom(vm => vm.Email));
		}
	}
}
