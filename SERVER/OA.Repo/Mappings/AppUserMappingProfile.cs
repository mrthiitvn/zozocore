﻿using AutoMapper;
using OA.Core.Models;
using OA.Infrastructure.EF.Models;

namespace OA.Repo.Mappings
{
    public class AppUserMappingProfile : Profile
    {
        public AppUserMappingProfile()
        {
            CreateMap<AppUserEntity, AppUser>().ForMember(au => au.UserName, map => map.MapFrom(vm => vm.Email)).ReverseMap();
        }
    }
}
