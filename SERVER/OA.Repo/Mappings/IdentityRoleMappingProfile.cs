﻿using AutoMapper;
using OA.Core.Models;
using OA.Infrastructure.EF.Models;

namespace OA.Repo.Mappings
{
    public class IdentityRoleMappingProfile : Profile
    {
        public IdentityRoleMappingProfile()
        {
            CreateMap<ApplicationRole, IdentityRoleEntity>();
        }
    }
}
