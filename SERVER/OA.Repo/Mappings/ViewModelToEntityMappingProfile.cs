﻿using AutoMapper;
using OA.Core.ViewModels;
using OA.Infrastructure.EF.Models;

namespace OA.Repo.Mappings
{
    public class ViewModelToEntityMappingProfile : Profile
    {
        public ViewModelToEntityMappingProfile()
        {
            CreateMap<RegistrationViewModel, AppUser>().ForMember(au => au.UserName, map => map.MapFrom(vm => vm.Email));
        }
    }
}
