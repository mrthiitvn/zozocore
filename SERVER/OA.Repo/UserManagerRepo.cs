﻿using OA.Core.Repoes;
using Microsoft.AspNetCore.Identity;
using OA.Infrastructure.EF.Models;
using System.Threading.Tasks;
using AutoMapper;
using OA.Infrastructure.EF.Context;
using System.Security.Claims;
using OA.Core.Factories;
using OA.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace OA.Repo
{
    public class UserManagerRepo : IUserManagerRepo
	{
		private readonly ApplicationDbContext _appDbContext;
		private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IMapper _mapper;
		private readonly IJwtFactory _jwtFactory;

		public UserManagerRepo(ApplicationDbContext applicationDbContext , UserManager<AppUser> userManager, RoleManager<ApplicationRole> roleManager
            , IMapper mapper, IJwtFactory jwtFactory)
		{
            _appDbContext = applicationDbContext;
            _userManager = userManager;
            _roleManager = roleManager;
			_mapper = mapper;
			_jwtFactory = jwtFactory;
		}

        #region - User -
        public async Task<bool> CreateIdentity(AppUserEntity appUserEntity)
        {
            var result = false;
            var mapUser = _mapper.Map<AppUser>(appUserEntity);
            var identityResult = await _userManager.CreateAsync(mapUser, appUserEntity.Password);
            if (identityResult.Succeeded)
            {
                await _appDbContext.Customers.AddAsync(new Customer { IdentityId = mapUser.Id, Location = appUserEntity.Location });
                await _appDbContext.SaveChangesAsync();

                //assign role default
                var user = await _userManager.FindByEmailAsync(appUserEntity.Email);
                await AddToRole(user);
                result = true;
            }
            return result;
        }

        public async Task<AppUserEntity> FindUserByEmail(string email)
        {
            AppUserEntity userEntity = null;
            if(!string.IsNullOrEmpty(email))
            {
                var user = await _userManager.FindByEmailAsync(email);
                if(user != null)
                {
                    userEntity = _mapper.Map<AppUserEntity>(user);
                }
            }
            return userEntity;
        }

        public async Task<AppUserEntity> FindUserByName(string userName)
        {
            AppUserEntity userEntity = null;
            if (!string.IsNullOrEmpty(userName))
            {
                var user = await _userManager.FindByNameAsync(userName);
                if (user != null)
                {
                    userEntity = _mapper.Map<AppUserEntity>(user);
                }
            }
            return userEntity;
        }

        public async Task<string> GetUserId(string userName)
        {
            var userId = string.Empty;
            if (!string.IsNullOrEmpty(userName))
            {
                var user = await _userManager.FindByNameAsync(userName);
                if (user != null)
                {
                    userId = user.Id;
                }
            }
            return userId;
        }

        public async Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password)
		{
			var result = new ClaimsIdentity();
			if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
			{
				result = await Task.FromResult<ClaimsIdentity>(null);
			}

			// get the user to verifty
			var userToVerify = await _userManager.FindByNameAsync(userName);
			if(userToVerify != null)
			{
				var isCheckPassword = await _userManager.CheckPasswordAsync(userToVerify, password);
				if (isCheckPassword)
				{
					result = await Task.FromResult(_jwtFactory.GenerateClaimsIdentity(userName, userToVerify.Id));
				}
			}
			return result;
		}

		public async Task<CustomerEntity> GetCustomer(Claim user)
		{
			CustomerEntity customerEntity = null;
			if (user != null)
			{
				var customer = await GetCustomerByUserId(user.Value);
                if (customer != null)
                {
                    customerEntity = _mapper.Map<CustomerEntity>(customer);
                }
			}
			return customerEntity;
		}

        public async Task<bool> UpdateUser(AppUserEntity appUserEntity)
        {
            var result = false;
            var user = await _userManager.FindByEmailAsync(appUserEntity.Email);
            if(user != null)
            {
                using (var transaction = _appDbContext.Database.BeginTransaction())
                {
                    var identity = await _userManager.UpdateAsync(user);
                    if (identity.Succeeded)
                    {
                        var currentData = await GetCustomerByUserId(user.Id);
                        if (currentData != null)
                        {
                            _appDbContext.Entry(currentData).CurrentValues.SetValues(currentData.Location = appUserEntity.Location);
                            await _appDbContext.SaveChangesAsync();
                            result = true;
                        }
                    }
                    transaction.Commit();
                }
            }

            return result;
        }

        public async Task<bool> DeleteUser(AppUserEntity appUserEntity)
        {
            var result = false;
            var user = await _userManager.FindByEmailAsync(appUserEntity.Email);
            if (user != null)
            {
                //user transaction for Rollback if delete has been fail
                using (var transaction = _appDbContext.Database.BeginTransaction())
                {
                    var currentCustomer = await GetCustomerByUserId(user.Id);
                    if (currentCustomer != null)
                    {
                        _appDbContext.Set<Customer>().Remove(currentCustomer);
                        await _appDbContext.SaveChangesAsync();
                        var identity = await _userManager.DeleteAsync(user);
                        result = identity.Succeeded;
                        transaction.Commit();
                    }
                }
            }

            return result;
        }

        private async Task<Customer> GetCustomerByUserId(string userId)
        {
            Customer customer = null;
            if (!string.IsNullOrEmpty(userId))
            {
                customer = await _appDbContext.Customers.Include(c => c.Identity).SingleOrDefaultAsync(c => c.Identity.Id == userId);
            }
            return customer;
        }
        #endregion

        #region - Role -
        public async Task<bool> AddRole(string role)
        {
            var result = false;
            var isRoleExist = await _roleManager.RoleExistsAsync(role);
            if (!isRoleExist)
            {
                var identityResult = await _roleManager.CreateAsync(new ApplicationRole(role));
                result = identityResult.Succeeded;
            }
            return result;
        }

        public async Task<bool> AssignRoleForUser(string userEmail, string role)
        {
            var result = false;
            var user = await _userManager.FindByEmailAsync(userEmail);
            if (user != null)
            {
                result = await AddToRole(user, role);
            }
            return result;
        }

        private async Task<bool> AddToRole(AppUser user, string role = "Member")
        {
            var result = false;
            var isRoleExist = await _roleManager.RoleExistsAsync(role);
            if (!isRoleExist)
            {
                await _roleManager.CreateAsync(new ApplicationRole(role));
            }
            var identityResult = await _userManager.AddToRoleAsync(user, role);
            return result == identityResult.Succeeded;
        }
        #endregion
    }
}
