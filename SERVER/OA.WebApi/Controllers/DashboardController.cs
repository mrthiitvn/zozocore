﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using OA.Core.Contants;
using OA.Core.Services;

namespace OA.WebApi.Controllers
{
	[Authorize(Policy = CommonConstants.Authorize.ApiUser)]
	[ApiVersion("1.0")]
	[Route(CommonConstants.Routes.BaseRoute)]
	public class DashboardController : Controller
	{
		private readonly ClaimsPrincipal _caller;
		private readonly IUserManagerService _userManagerService;

		public DashboardController(IHttpContextAccessor httpContextAccessor, IUserManagerService userManagerService)
		{
			_caller = httpContextAccessor.HttpContext.User;
			_userManagerService = userManagerService;
		}

		// GET api/dashboard/home
		[HttpGet]
		public async Task<IActionResult> Home()
		{
			ObjectResult result;
			var userId = _caller.Claims.SingleOrDefault(c => c.Type == "id");
			if (userId == null)
			{
				result = new ObjectResult(BadRequest());
			}
			else
			{
				var customer = await _userManagerService.GetCustomer(userId);
				if (customer == null)
				{
					result = new BadRequestObjectResult("Customer not found!");
				}
				else
				{
					result = new OkObjectResult(
						new
						{
							Message = "This is secure API and user data!",
							customer.Identity.FirstName,
							customer.Identity.LastName,
							customer.Identity.PictureUrl,
							customer.Identity.FacebookId,
							customer.Location,
							customer.Locale,
							customer.Gender
						});
				}
			}
			return result;
		}
	}
}