﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using OA.Core.Contants;
using OA.Core.Factories;
using OA.Core.Models;
using OA.Core.Services;
using OA.Core.ViewModels;
using OA.Service.Helpers;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace OA.WebApi.Controllers
{
    [ApiVersion("1.0")]
	[Route(CommonConstants.Routes.BaseRoute)]
	public class ExternalAuthController : Controller
	{
        private readonly IUserManagerService _userManagerService;
		private readonly FacebookAuthSettings _fbAuthSettings;
		private readonly IJwtFactory _jwtFactory;
		private readonly JwtIssuerOptions _jwtOptions;
		private static readonly HttpClient Client = new HttpClient();

		public ExternalAuthController(IUserManagerService userManagerService, IOptions<FacebookAuthSettings> fbAuthSettingsAccessor
            , IJwtFactory jwtFactory, IOptions<JwtIssuerOptions> jwtOptions)
		{
            _userManagerService = userManagerService;
            _fbAuthSettings = fbAuthSettingsAccessor.Value;
			_jwtFactory = jwtFactory;
			_jwtOptions = jwtOptions.Value;
		}

		// POST api/externalauth/facebook
		[HttpPost]
		public async Task<IActionResult> Facebook([FromBody]FacebookAuthViewModel model)
		{
            ObjectResult result;
            // 1.generate an app access token
            var appAccessTokenResponse = await Client.GetStringAsync(string.Format(CommonConstants.FacebookAuthentication.AccessTokenResponseUri
                , _fbAuthSettings.AppId
                , _fbAuthSettings.AppSecret));
                //await Client.GetStringAsync($"https://graph.facebook.com/oauth/access_token?client_id={_fbAuthSettings.AppId}&client_secret={_fbAuthSettings.AppSecret}&grant_type=client_credentials");
            var appAccessToken = JsonConvert.DeserializeObject<FacebookAppAccessToken>(appAccessTokenResponse);
            // 2. validate the user access token
            var userAccessTokenValidationResponse = await Client.GetStringAsync(string.Format(CommonConstants.FacebookAuthentication.AccessTokenValidationResponseUri
                , model.AccessToken, appAccessToken.AccessToken));
                //await Client.GetStringAsync($"https://graph.facebook.com/debug_token?input_token={model.AccessToken}&access_token={appAccessToken.AccessToken}");
            var userAccessTokenValidation = JsonConvert.DeserializeObject<FacebookUserAccessTokenValidation>(userAccessTokenValidationResponse);

            if (!userAccessTokenValidation.Data.IsValid)
            {
                result = new BadRequestObjectResult(BadRequest(Errors.AddErrorToModelState("login_failure", "Invalid facebook token.", ModelState)));
                //return BadRequest(Errors.AddErrorToModelState("login_failure", "Invalid facebook token.", ModelState));
            }
            else
            {
                // 3. we've got a valid token so we can request user data from fb
                var userInfoResponse = await Client.GetStringAsync(string.Format(CommonConstants.FacebookAuthentication.UserInfoResponseUri, model.AccessToken));
                    //await Client.GetStringAsync($"https://graph.facebook.com/v2.8/me?fields=id,email,first_name,last_name,name,gender,locale,birthday,picture&access_token={model.AccessToken}");
                var userInfo = JsonConvert.DeserializeObject<FacebookUserData>(userInfoResponse);

                // 4. ready to create the local user account (if necessary) and jwt
                var userEntity = await _userManagerService.FindUserByEmail(userInfo.Email);//_userManager.FindByEmailAsync(userInfo.Email);
                if (userEntity == null)
                {
                    var newUserEntity = new AppUserEntity
                    {
                        FirstName = userInfo.FirstName,
                        LastName = userInfo.LastName,
                        FacebookId = userInfo.Id,
                        Email = userInfo.Email,
                        //UserName = userInfo.Email,
                        PictureUrl = userInfo.Picture.Data.Url,
                        Password = Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Substring(0, 8)
                    };

                    var isCreateUser = await _userManagerService.CreateIdentity(newUserEntity);
                    if(!isCreateUser)
                    {
                        return new BadRequestObjectResult(ModelState);
                    }
                    //var identityResult = await _userManager.CreateAsync(newUser, Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Substring(0, 8));

                    //if (!identityResult.Succeeded) return new BadRequestObjectResult(Errors.AddErrorsToModelState(identityResult, ModelState));

                    //await _appDbContext.Customers.AddAsync(new Customer { IdentityId = newUser.Id, Location = "", Locale = userInfo.Locale, Gender = userInfo.Gender });
                    //await _appDbContext.SaveChangesAsync();
                }

                // generate the jwt for the local user...
                var localUser = await _userManagerService.FindUserByName(userInfo.Email);//_userManager.FindByNameAsync(userInfo.Email);

                if (localUser == null)
                {
                    result = new BadRequestObjectResult(BadRequest(Errors.AddErrorToModelState("login_failure", "Failed to create local user account.", ModelState)));
                    //return BadRequest(Errors.AddErrorToModelState("login_failure", "Failed to create local user account.", ModelState));
                }
                else
                {
                    var userId = await _userManagerService.GetUserId(userInfo.Email);
                    var jwt = await Tokens.GenerateJwt(_jwtFactory.GenerateClaimsIdentity(localUser.Email, userId),
                        _jwtFactory, localUser.Email, _jwtOptions, new JsonSerializerSettings { Formatting = Formatting.Indented });
                    result = new OkObjectResult(jwt);
                    //var jwt = await Tokens.GenerateJwt(_jwtFactory.GenerateClaimsIdentity(localUser.UserName, localUser.Id),
                    // _jwtFactory, localUser.UserName, _jwtOptions, new JsonSerializerSettings { Formatting = Formatting.Indented });

                    //return new OkObjectResult(jwt);
                }
            }
            return result;
		}
	}
}