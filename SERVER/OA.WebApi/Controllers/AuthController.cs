﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OA.Core.Factories;
using OA.Core.Models;
using Microsoft.Extensions.Options;
using OA.Service.Helpers;
using Newtonsoft.Json;
using OA.Core.Contants;
using OA.Core.ViewModels;
using OA.Core.Services;

namespace OA.WebApi.Controllers
{
	[ApiVersion("1.0")]
	[Route(CommonConstants.Routes.BaseRoute)]
	public class AuthController : Controller
	{
		private readonly IUserManagerService _userManagerService;
		private readonly IJwtFactory _jwtFactory;
		private readonly JwtIssuerOptions _jwtOptions;

		public AuthController(IUserManagerService userManagerService, IJwtFactory jwtFactory, IOptions<JwtIssuerOptions> jwtOptions)
		{
			_userManagerService = userManagerService;
			_jwtFactory = jwtFactory;
			_jwtOptions = jwtOptions.Value;
		}

		// POST api/auth/login
		[HttpPost]
		public async Task<IActionResult> Login([FromBody]CredentialsViewModel credentials)
		{
			ObjectResult result;
			if (!ModelState.IsValid)
			{
				result = new ObjectResult(BadRequest(ModelState));
			}
			else
			{
				var identity = await _userManagerService.GetClaimsIdentity(credentials.UserName, credentials.Password);
				if (identity == null)
				{
					result = new BadRequestObjectResult(
						BadRequest(Errors.AddErrorToModelState("login_failure", "Invalid username or password.", ModelState)));
				}
				else
				{
					var jwt = await Tokens.GenerateJwt(identity, _jwtFactory
						, credentials.UserName
						, _jwtOptions
						, new JsonSerializerSettings { Formatting = Formatting.Indented });
					result = new OkObjectResult(jwt);
				}
			}
			return result;
		}
	}
}