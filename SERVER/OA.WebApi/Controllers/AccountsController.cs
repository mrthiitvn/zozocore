﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OA.Core.Contants;
using OA.Core.Services;
using OA.Core.Models;

namespace OA.WebApi.Controllers
{
    [ApiVersion("1.0")]
	[Route(CommonConstants.Routes.BaseRoute)]
	public class AccountsController : Controller
	{
		private readonly IUserManagerService _userManagerService;

		public AccountsController(IUserManagerService userManagerService)
		{
			_userManagerService = userManagerService;

		}

		// POST api/accounts
		[HttpPost]
		public async Task<IActionResult> CreateUser([FromBody]AppUserEntity model)
		{
			ObjectResult result;
			if (!ModelState.IsValid)
			{
				result = new ObjectResult(BadRequest(ModelState));
			}
			else
			{
				var isCreateIdentity = await _userManagerService.CreateIdentity(model);
				if (!isCreateIdentity)
				{
					result = new BadRequestObjectResult("Creat account error");
				}
				else
				{
					result = new OkObjectResult("Account created");
				}
			}
			return result;
		}

        [HttpPost]
        public async Task<IActionResult> UpdateUser([FromBody] AppUserEntity appUserEntity)
        {
            ObjectResult result;
            if (!ModelState.IsValid)
            {
                result = new ObjectResult(BadRequest(ModelState));
            }
            else
            {
                var isUpdateUser = await _userManagerService.UpdateUser(appUserEntity);
                if(!isUpdateUser)
                {
                    result = new BadRequestObjectResult("Update account error!");
                }
                else
                {
                    result = new OkObjectResult("Account Updated!");
                }
            }
            return result;
        }

        [HttpPost]
        public async Task<IActionResult> DeleteUser([FromBody] AppUserEntity appUserEntity)
        {
            ObjectResult result;
            if (!ModelState.IsValid)
            {
                result = new ObjectResult(BadRequest(ModelState));
            }
            else
            {
                var isDeleteUser = await _userManagerService.DeleteUser(appUserEntity);
                if (!isDeleteUser)
                {
                    result = new BadRequestObjectResult("Delete account error!");
                }
                else
                {
                    result = new OkObjectResult("Account Deleted!");
                }
            }
            return result;
        }

        [HttpGet(CommonConstants.Routes.AddNewRole)]
        public async Task<IActionResult> AddRole(string role)
        {
            ObjectResult result;
            if (string.IsNullOrEmpty(role))
            {
                result = new ObjectResult(BadRequest("Role incorrect!"));
            }
            else
            {
                var isCreate = await _userManagerService.AddRole(role);
                if(!isCreate)
                {
                    result = new BadRequestObjectResult("Add role error!");
                }
                else
                {
                    result = new OkObjectResult("Added Role!");
                }
            }
            return result;
        }

        [HttpGet(CommonConstants.Routes.AssignRole)]
        public async Task<IActionResult> AssignRoleForUser(string userEmail, string role)
        {
            ObjectResult result;
            if (string.IsNullOrEmpty(role) || string.IsNullOrEmpty(userEmail))
            {
                result = new ObjectResult(BadRequest("Role or user incorrect!"));
            }
            else
            {
                var isAssign = await _userManagerService.AssignRoleForUser(userEmail, role);
                if(!isAssign)
                {
                    result = new BadRequestObjectResult("Add role for error!");
                }
                else
                {
                    result = new OkObjectResult("Added role!");
                }
            }
            return result;
        }
    }
}