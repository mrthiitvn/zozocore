﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using OA.Infrastructure.EF.Context;
using Microsoft.EntityFrameworkCore;
using OA.Core.Factories;
using OA.Service.Factories;
using OA.Core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using OA.Core.Contants;
using OA.Infrastructure.EF.Models;
using Microsoft.AspNetCore.Identity;
using AutoMapper;
using System.Text;
using Microsoft.AspNetCore.Diagnostics;
using System.Net;
using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection.Extensions;
using OA.Service.Helpers;
using OA.Core.Services;
using OA.Service;
using OA.Core.Repoes;
using OA.Repo;
using OA.Repo.Mappings;
using Microsoft.AspNetCore.Mvc;

namespace WebApi
{
	public class Startup
	{
		private const string SecretKey = "iNivDmHLpUA223sqsfhqGbMRdRj1PVkH"; // todo: get this from somewhere secure
		private readonly SymmetricSecurityKey _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}
		public IConfiguration Configuration { get; }
		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			//Swagger define
			services.AddSwaggerGen(options =>
			{
				options.SwaggerDoc("v1", new Info
				{
					Title = PlatformServices.Default.Application.ApplicationName,
					Version = PlatformServices.Default.Application.ApplicationVersion
				});
			});
			#region --- JWT ---
			// Add framework services.
			services.AddDbContext<ApplicationDbContext>(options =>
				options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
					b => b.MigrationsAssembly("OA.Infrastructure.EF")));
			services.AddSingleton<IJwtFactory, JwtFactory>();
			// Register the ConfigurationBuilder instance of FacebookAuthSettings
			services.Configure<FacebookAuthSettings>(Configuration.GetSection(nameof(FacebookAuthSettings)));
			services.TryAddTransient<IHttpContextAccessor, HttpContextAccessor>();
			// jwt wire up
			// Get options from app settings
			var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));
			// Configure JwtIssuerOptions
			services.Configure<JwtIssuerOptions>(options =>
			{
				options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
				options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
				options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
			});
			var tokenValidationParameters = new TokenValidationParameters
			{
				ValidateIssuer = true,
				ValidIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)],
				ValidateAudience = true,
				ValidAudience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)],
				ValidateIssuerSigningKey = true,
				IssuerSigningKey = _signingKey,
				RequireExpirationTime = false,
				ValidateLifetime = true,
				ClockSkew = TimeSpan.Zero
			};
			services.AddAuthentication(options =>
			{
				options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
			}).AddJwtBearer(configureOptions =>
			{
				configureOptions.ClaimsIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
				configureOptions.TokenValidationParameters = tokenValidationParameters;
				configureOptions.SaveToken = true;
			});
			// api user claim policy
			services.AddAuthorization(options =>
			{
				options.AddPolicy("ApiUser", policy => policy.RequireClaim(ConstantsJWT.Strings.JwtClaimIdentifiers.Rol, ConstantsJWT.Strings.JwtClaims.ApiAccess));
			});
			// add identity
			var builder = services.AddIdentityCore<AppUser>(o =>
			{
				// configure identity options
				o.Password.RequireDigit = false;
				o.Password.RequireLowercase = false;
				o.Password.RequireUppercase = false;
				o.Password.RequireNonAlphanumeric = false;
				o.Password.RequiredLength = 6;
			});
			builder = new IdentityBuilder(builder.UserType, typeof(ApplicationRole), builder.Services);
			builder.AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();
			//services.AddAutoMapper();
			services.AddMvc().AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>());
			#endregion

			//add version
			services.AddApiVersioning(o => {
				o.ReportApiVersions = true;
				o.AssumeDefaultVersionWhenUnspecified = true;
				o.DefaultApiVersion = new ApiVersion(1, 0);
			});

            //add role identity
            services.AddIdentity<AppUser, ApplicationRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            AddRepositories(services);
			AddServices(services);
			RegisterMapper(services);
		}
		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				//
			}
			//Swagger using
			app.UseSwagger();
			app.UseSwaggerUI(options =>
			{
				options.SwaggerEndpoint("/swagger/v1/swagger.json",
					$"{PlatformServices.Default.Application.ApplicationName} {PlatformServices.Default.Application.ApplicationVersion}");
			});
			#region --- JWT ---
			app.UseExceptionHandler(
				  builder =>
				  {
					  builder.Run(
								async context =>
								{
									context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
									context.Response.Headers.Add("Access-Control-Allow-Origin", "*");
									var error = context.Features.Get<IExceptionHandlerFeature>();
									if (error != null)
									{
										context.Response.AddApplicationError(error.Error.Message);
										await context.Response.WriteAsync(error.Error.Message).ConfigureAwait(false);
									}
								});
				  });
			app.UseAuthentication();
			#endregion

			app.UseDefaultFiles();
			app.UseStaticFiles();
			app.UseMvc();
		}
		private static void AddServices(IServiceCollection services)
		{
			services.AddTransient<IUserManagerService, UserManagerService>();
		}
		private static void AddRepositories(IServiceCollection services)
		{
			services.AddTransient<IUserManagerRepo, UserManagerRepo>();
		}

		private static void RegisterMapper(IServiceCollection services)
		{
			var config = new MapperConfiguration(cfg =>
			{
				cfg.AddProfile(new ViewModelToEntityMappingProfile());
				cfg.AddProfile(new CustomerMappingProfile());
                cfg.AddProfile(new AppUserMappingProfile());
                cfg.AddProfile(new IdentityRoleMappingProfile());
			});

			var mapper = config.CreateMapper();
			services.AddSingleton(mapper);
		}
	}
}
