﻿using OA.Core.Services;
using System.Threading.Tasks;
using OA.Core.Repoes;
using System.Security.Claims;
using OA.Core.Models;

namespace OA.Service
{
    public class UserManagerService : IUserManagerService
	{
		private readonly IUserManagerRepo _userManagerRepo;
		public UserManagerService(IUserManagerRepo userManagerRepo)
		{
			_userManagerRepo = userManagerRepo;
		}

		//public async Task<bool> CreateIdentity(RegistrationViewModel registrationViewModel)
		//{
		//	return await _userManagerRepo.CreateIdentity(registrationViewModel);
		//}

        public async Task<bool> CreateIdentity(AppUserEntity appUserEntity)
        {
            return await _userManagerRepo.CreateIdentity(appUserEntity);
        }

        public async Task<AppUserEntity> FindUserByEmail(string email)
        {
            return await _userManagerRepo.FindUserByEmail(email);
        }

        public async Task<AppUserEntity> FindUserByName(string userName)
        {
            return await _userManagerRepo.FindUserByName(userName);
        }

        public async Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password)
		{
			return await _userManagerRepo.GetClaimsIdentity(userName, password);
		}

		public async Task<CustomerEntity> GetCustomer(Claim user)
		{
			return await _userManagerRepo.GetCustomer(user);
		}

        public async Task<string> GetUserId(string userName)
        {
            return await _userManagerRepo.GetUserId(userName);
        }

        public async Task<bool> UpdateUser(AppUserEntity appUserEntity)
        {
            return await _userManagerRepo.UpdateUser(appUserEntity);
        }

        public async Task<bool> DeleteUser(AppUserEntity appUserEntity)
        {
            return await _userManagerRepo.DeleteUser(appUserEntity);
        }

        #region - Role -
        public async Task<bool> AddRole(string role)
        {
            return await _userManagerRepo.AddRole(role);
        }

        public async Task<bool> AssignRoleForUser(string userEmail, string role)
        {
            return await _userManagerRepo.AssignRoleForUser(userEmail, role);
        }
        #endregion
    }
}
