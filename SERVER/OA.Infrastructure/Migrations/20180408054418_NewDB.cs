﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace OA.Infrastructure.EF.Migrations
{
    public partial class NewDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    FacebookId = table.Column<long>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    PasswordHash = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    PictureUrl = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CT_CATRUC",
                columns: table => new
                {
                    CT_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CT_GHICHU = table.Column<string>(nullable: true),
                    CT_NGAYTRUC = table.Column<DateTime>(type: "datetime", nullable: true),
                    CT_TEN = table.Column<string>(maxLength: 250, nullable: true),
                    CT_TG_BATDAU = table.Column<DateTime>(type: "datetime", nullable: true),
                    CT_TG_KETTHUC = table.Column<DateTime>(type: "datetime", nullable: true),
                    ND_ID = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CT_CATRUC", x => x.CT_ID);
                });

            migrationBuilder.CreateTable(
                name: "CT_KN_HIENTRUONG",
                columns: table => new
                {
                    KNHT_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    KNHT_GHICHU = table.Column<string>(nullable: true),
                    KNHT_KQ_XULY = table.Column<bool>(nullable: false),
                    KNHT_NOIDUNG_TT = table.Column<string>(nullable: true),
                    KNHT_STT = table.Column<int>(nullable: true),
                    KNHT_THOIGIAN = table.Column<DateTime>(type: "datetime", nullable: true),
                    TV_ID = table.Column<long>(nullable: false),
                    XDVV_ID = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CT_KN_HIENTRUONG", x => x.KNHT_ID);
                });

            migrationBuilder.CreateTable(
                name: "CT_TOGIAC",
                columns: table => new
                {
                    TG_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TG_DIACHI_COQUAN = table.Column<string>(maxLength: 250, nullable: true),
                    TG_HO = table.Column<string>(maxLength: 250, nullable: true),
                    TG_TEN = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CT_TOGIAC", x => x.TG_ID);
                });

            migrationBuilder.CreateTable(
                name: "CT_TOIPHAM",
                columns: table => new
                {
                    TP_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TP_DIACHI = table.Column<string>(maxLength: 250, nullable: true),
                    TP_DIADIEM = table.Column<string>(maxLength: 250, nullable: true),
                    TP_HO = table.Column<string>(maxLength: 250, nullable: true),
                    TP_NOIDUNG = table.Column<string>(nullable: false),
                    TP_TEN = table.Column<string>(maxLength: 250, nullable: true),
                    TP_THOIGIAN = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CT_TOIPHAM", x => x.TP_ID);
                });

            migrationBuilder.CreateTable(
                name: "DONVI",
                columns: table => new
                {
                    DV_ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    DV_DIENGIAI = table.Column<string>(nullable: true),
                    DV_SAPXEP = table.Column<int>(nullable: true),
                    DV_TEN = table.Column<string>(maxLength: 250, nullable: false),
                    DV_TEN_VIETTAT = table.Column<string>(maxLength: 50, nullable: true),
                    LastEdit = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastEditBy = table.Column<string>(maxLength: 250, nullable: true),
                    Status = table.Column<int>(nullable: true, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DONVI", x => x.DV_ID);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Gender = table.Column<string>(nullable: true),
                    IdentityId = table.Column<string>(nullable: true),
                    Locale = table.Column<string>(nullable: true),
                    Location = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customers_AspNetUsers_IdentityId",
                        column: x => x.IdentityId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CT_CHUYENCA",
                columns: table => new
                {
                    CC_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CC_THOIGIAN = table.Column<DateTime>(type: "datetime", nullable: true),
                    CT_ID_FROM = table.Column<long>(nullable: false),
                    CT_ID_TO = table.Column<long>(nullable: false),
                    TV_ID = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CT_CHUYENCA", x => x.CC_ID);
                    table.ForeignKey(
                        name: "FK_CT_CHUYENCA_CT_CATRUC",
                        column: x => x.CT_ID_FROM,
                        principalTable: "CT_CATRUC",
                        principalColumn: "CT_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CT_CHUYENCA_CT_CATRUC1",
                        column: x => x.CT_ID_TO,
                        principalTable: "CT_CATRUC",
                        principalColumn: "CT_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CT_KYNHAN",
                columns: table => new
                {
                    KN_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CT_ID = table.Column<long>(nullable: false),
                    KN_THOIGIAN = table.Column<DateTime>(type: "datetime", nullable: true),
                    KN_TINHHINH = table.Column<string>(nullable: true),
                    TV_ID = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CT_KYNHAN", x => x.KN_ID);
                    table.ForeignKey(
                        name: "FK_CT_KYNHAN_CT_CATRUC",
                        column: x => x.CT_ID,
                        principalTable: "CT_CATRUC",
                        principalColumn: "CT_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CT_PHANCONG",
                columns: table => new
                {
                    PC_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CT_ID = table.Column<long>(nullable: false),
                    PC_THOIGIAN = table.Column<DateTime>(type: "datetime", nullable: true),
                    TV_ID_THANHVIEN = table.Column<long>(nullable: false),
                    TV_ID_TRUONGCA = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CT_PHANCONG", x => x.PC_ID);
                    table.ForeignKey(
                        name: "FK_CT_PHANCONG_CT_CATRUC",
                        column: x => x.CT_ID,
                        principalTable: "CT_CATRUC",
                        principalColumn: "CT_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CT_NOIDUNG",
                columns: table => new
                {
                    ND_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    KNHT_ID = table.Column<long>(nullable: false),
                    ND_KHAC = table.Column<string>(nullable: true),
                    TB_ID = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CT_NOIDUNG", x => x.ND_ID);
                    table.ForeignKey(
                        name: "FK_CT_NOIDUNG_CT_KN_HIENTRUONG",
                        column: x => x.KNHT_ID,
                        principalTable: "CT_KN_HIENTRUONG",
                        principalColumn: "KNHT_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CT_THANHVIEN",
                columns: table => new
                {
                    TV_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DV_ID = table.Column<int>(nullable: false),
                    TV_CHUCVU = table.Column<string>(maxLength: 250, nullable: true),
                    TV_DIEUTRAVIEN = table.Column<bool>(nullable: false),
                    TV_HO = table.Column<string>(maxLength: 250, nullable: true),
                    TV_KIEMSOATVIEN = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    TV_SDT = table.Column<string>(maxLength: 250, nullable: true),
                    TV_TEN = table.Column<string>(maxLength: 250, nullable: true),
                    TV_THUTU = table.Column<int>(nullable: true),
                    TV_TRUONGCA = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CT_THANHVIEN", x => x.TV_ID);
                    table.ForeignKey(
                        name: "FK_CT_THANHVIEN_DONVI",
                        column: x => x.DV_ID,
                        principalTable: "DONVI",
                        principalColumn: "DV_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CT_TINBAO",
                columns: table => new
                {
                    TB_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CT_ID = table.Column<long>(nullable: true),
                    TB_GHICHU = table.Column<string>(nullable: true),
                    TB_ND_XULY = table.Column<string>(nullable: true),
                    TB_STT = table.Column<int>(nullable: true),
                    TB_THOIGIAN_TIEPNHAN = table.Column<DateTime>(type: "datetime", nullable: true),
                    TB_THOIGIAN_XULY = table.Column<DateTime>(type: "datetime", nullable: true),
                    TG_ID = table.Column<long>(nullable: false),
                    TP_ID = table.Column<long>(nullable: false),
                    TV_ID = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CT_TINBAO", x => x.TB_ID);
                    table.ForeignKey(
                        name: "FK_CT_TINBAO_CT_THANHVIEN",
                        column: x => x.TV_ID,
                        principalTable: "CT_THANHVIEN",
                        principalColumn: "TV_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CT_XACDINH_VUVIEC",
                columns: table => new
                {
                    XDVV_ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TV_ID = table.Column<long>(nullable: false),
                    XDVV_DAUHIEU_PHAMTOI = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CT_XACDINH_VUVIEC", x => x.XDVV_ID);
                    table.ForeignKey(
                        name: "FK_CT_XACDINH_VUVIEC_CT_THANHVIEN",
                        column: x => x.TV_ID,
                        principalTable: "CT_THANHVIEN",
                        principalColumn: "TV_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_CT_CHUYENCA_CT_ID_FROM",
                table: "CT_CHUYENCA",
                column: "CT_ID_FROM");

            migrationBuilder.CreateIndex(
                name: "IX_CT_CHUYENCA_CT_ID_TO",
                table: "CT_CHUYENCA",
                column: "CT_ID_TO");

            migrationBuilder.CreateIndex(
                name: "IX_CT_KYNHAN_CT_ID",
                table: "CT_KYNHAN",
                column: "CT_ID");

            migrationBuilder.CreateIndex(
                name: "IX_CT_NOIDUNG_KNHT_ID",
                table: "CT_NOIDUNG",
                column: "KNHT_ID");

            migrationBuilder.CreateIndex(
                name: "IX_CT_PHANCONG_CT_ID",
                table: "CT_PHANCONG",
                column: "CT_ID");

            migrationBuilder.CreateIndex(
                name: "IX_CT_THANHVIEN_DV_ID",
                table: "CT_THANHVIEN",
                column: "DV_ID");

            migrationBuilder.CreateIndex(
                name: "IX_CT_TINBAO_TV_ID",
                table: "CT_TINBAO",
                column: "TV_ID");

            migrationBuilder.CreateIndex(
                name: "IX_CT_XACDINH_VUVIEC_TV_ID",
                table: "CT_XACDINH_VUVIEC",
                column: "TV_ID");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_IdentityId",
                table: "Customers",
                column: "IdentityId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "CT_CHUYENCA");

            migrationBuilder.DropTable(
                name: "CT_KYNHAN");

            migrationBuilder.DropTable(
                name: "CT_NOIDUNG");

            migrationBuilder.DropTable(
                name: "CT_PHANCONG");

            migrationBuilder.DropTable(
                name: "CT_TINBAO");

            migrationBuilder.DropTable(
                name: "CT_TOGIAC");

            migrationBuilder.DropTable(
                name: "CT_TOIPHAM");

            migrationBuilder.DropTable(
                name: "CT_XACDINH_VUVIEC");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "CT_KN_HIENTRUONG");

            migrationBuilder.DropTable(
                name: "CT_CATRUC");

            migrationBuilder.DropTable(
                name: "CT_THANHVIEN");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "DONVI");
        }
    }
}
