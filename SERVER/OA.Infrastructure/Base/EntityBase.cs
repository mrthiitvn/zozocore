﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OA.Infrastructure.EF.Base
{
	public abstract class EntityBase
	{
		public long Id { get; set; }
		//
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		public DateTime? CreatedDate { get; set; }
		//
		[DataType(DataType.Date)]
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		public DateTime? UpdatedDate { get; set; }
		//
		public string CreatedBy { get; set; }
		public string UpdatedBy { get; set; }

	}
}
