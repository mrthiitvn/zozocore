﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
namespace OA.Infrastructure.SQL.Common
{
	public class CommonDb
    {
		protected string ConnectionString { get; set; }
		public SqlConnection GetConnection()
		{
			SqlConnection connection = new SqlConnection(ConnectionString);
			if (connection.State != ConnectionState.Open)
				connection.Open();
			return connection;
		}
		protected SqlCommand GetCommand(DbConnection connection, string commandText, CommandType commandType)
		{
			SqlCommand command = new SqlCommand(commandText, connection as SqlConnection);
			command.CommandType = commandType;
			return command;
		}
		protected SqlParameter GetParameter(string parameter, object value)
		{
			SqlParameter parameterObject = new SqlParameter(parameter, value != null ? value : DBNull.Value);
			parameterObject.Direction = ParameterDirection.Input;
			return parameterObject;
		}
		protected SqlParameter GetParameterOut(string parameter, SqlDbType type, object value = null, ParameterDirection parameterDirection = ParameterDirection.InputOutput)
		{
			SqlParameter parameterObject = new SqlParameter(parameter, type);
			if (type == SqlDbType.NVarChar || type == SqlDbType.VarChar || type == SqlDbType.NText || type == SqlDbType.Text)
			{
				parameterObject.Size = -1;
			}
			parameterObject.Direction = parameterDirection;
			if (value != null)
			{
				parameterObject.Value = value;
			}
			else
			{
				parameterObject.Value = DBNull.Value;
			}
			return parameterObject;
		}
		protected int ExecuteNonQuery(string procedureName, List<DbParameter> parameters, CommandType commandType = CommandType.StoredProcedure)
		{
			int returnValue = -1;
			using (SqlConnection connection = GetConnection())
			{
				try
				{
					SqlCommand cmd = GetCommand(connection, procedureName, commandType);
					if (parameters != null && parameters.Count > 0)
					{
						cmd.Parameters.AddRange(parameters.ToArray());
					}
					returnValue = cmd.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					throw new InvalidOperationException(ex.Message);
				}
				finally
				{
					if (connection.State != ConnectionState.Closed)
					{
						connection.Close();
					}
				}
			}
			return returnValue;
		}
		protected object ExecuteScalar(string procedureName, List<DbParameter> parameters)
		{
			object returnValue = null;
			using (SqlConnection connection = GetConnection())
			{
				SqlCommand cmd = GetCommand(connection, procedureName, CommandType.StoredProcedure);
				if (parameters != null && parameters.Count > 0)
				{
					cmd.Parameters.AddRange(parameters.ToArray());
				}
				returnValue = cmd.ExecuteScalar();
			}
			return returnValue;
		}
		protected IDataReader ExecuteReader(string procedureName, List<DbParameter> parameters, CommandType commandType = CommandType.StoredProcedure)
		{
			DataTable dt = new DataTable();
			using (SqlConnection connection = GetConnection())
			{
				try
				{
					SqlCommand cmd = GetCommand(connection, procedureName, commandType);
					if (parameters != null && parameters.Count > 0)
					{
						cmd.Parameters.AddRange(parameters.ToArray());
					}
					SqlDataReader sqlDr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
					dt.Load(sqlDr);
				}
				catch (Exception ex)
				{
					throw new InvalidOperationException(ex.Message);
				}
				finally
				{
					if (connection.State != ConnectionState.Closed)
					{
						connection.Close();
					}
				}
			}
			return dt.CreateDataReader();
		}
		protected DataTable ExecuteTable(string procedureName, List<DbParameter> parameters, CommandType commandType = CommandType.StoredProcedure)
		{
			DataTable dt = new DataTable();
			using (SqlConnection connection = GetConnection())
			{
				try
				{
					DataSet ds = new DataSet();
					SqlCommand cmd = GetCommand(connection, procedureName, commandType);
					if (parameters != null && parameters.Count > 0)
					{
						cmd.Parameters.AddRange(parameters.ToArray());
					}
					var sqlDa = new SqlDataAdapter(cmd);
					sqlDa.Fill(ds);
					dt = ds.Tables[0];
				}
				catch (Exception ex)
				{
					throw new InvalidOperationException(ex.Message);
				}
				finally
				{
					if (connection.State != ConnectionState.Closed)
					{
						connection.Close();
					}
				}
			}
			return dt;
		}
		protected DataSet ExecuteDataSet(string procedureName, List<DbParameter> parameters, CommandType commandType = CommandType.StoredProcedure)
		{
			DataSet ds = new DataSet();
			using (SqlConnection connection = GetConnection())
			{
				try
				{
					SqlCommand cmd = GetCommand(connection, procedureName, commandType);
					if (parameters != null && parameters.Count > 0)
					{
						cmd.Parameters.AddRange(parameters.ToArray());
					}
					var sqlDa = new SqlDataAdapter(cmd);
					sqlDa.Fill(ds);
				}
				catch (Exception ex)
				{
					throw new InvalidOperationException(ex.Message);
				}
				finally
				{
					if (connection.State != ConnectionState.Closed)
					{
						connection.Close();
					}
				}
			}
			return ds;
		}
	}
}
