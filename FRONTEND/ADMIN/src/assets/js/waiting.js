var CustomWaiting = (function () {
    return {

        blockUI: function () {
            $.blockUI({ message: '<div style="width:100%;"><img src="assets/img/wait.gif" /></div>', css: { 'padding': '10px', 'background': 'transparent', 'border': '0px', 'margin': 'auto' } });
        },
        unblockUI: function () {
            $.unblockUI();
        }
    }
})(CustomWaiting || {})