import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { AppComponent } from './app.component';
import { InformMessageComponent } from './shared/modal/inform-message/inform-message.component';
import { CommonService } from './shared/services/common.service';
import { LayoutModule } from './layout/layout.module';


@NgModule({
  declarations: [
    AppComponent,
    InformMessageComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    BootstrapModalModule,
    AppRoutingModule,
    LayoutModule,
  ],
  providers: [CommonService],
  entryComponents: [InformMessageComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
