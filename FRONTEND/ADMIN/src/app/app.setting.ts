export class AppSetting {
    //server: local
    public apiUrl = 'http://localhost:57392/api/';

    /********** Test **********/
    public getTest = this.apiUrl + "Test/gettest";
    public getTests = this.apiUrl + "Test/gettests";
    public addTest = this.apiUrl + "Test/addtest";
    public editTest = this.apiUrl + "Test/edittest";
    public deleteTest = this.apiUrl + "Test/deletetest";
}
