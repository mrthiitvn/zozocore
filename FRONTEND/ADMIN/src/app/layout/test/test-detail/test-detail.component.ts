import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { TestService } from '../../../shared/services/test.service';
import { CommonService } from '../../../shared/services/common.service';
import { TestModel } from '../test-model';
import { InformMessageComponent } from '../../../shared/modal/inform-message/inform-message.component';

declare var CustomWaiting: any;

export interface DetailModel {
  testId: string;
}

@Component({
  selector: 'app-test-detail',
  templateUrl: './test-detail.component.html',
  styleUrls: ['./test-detail.component.scss']
})
export class TestDetailComponent extends DialogComponent<DetailModel, null> implements OnInit, DetailModel {

  testId: string;
  public test: any;

  constructor(dialogService: DialogService, public testService: TestService,
    private commonService: CommonService) {
    super(dialogService);
  }

  ngOnInit() {
    this.loadTest();
  }

  loadTest() {
    CustomWaiting.blockUI();
    if (this.testId !== "") {
      this.testService.getTest(this.testId)
        .subscribe(
        response => {
          let res = response['value'];
          if (res.status)
            this.test = res.data;
          else
            this.dialogService.addDialog(InformMessageComponent, { title: 'Load Test', message: res.message });
          CustomWaiting.unblockUI();
        },
        error => {
          this.commonService.handleError(error);
          CustomWaiting.unblockUI();
        });
    }
  }
}
