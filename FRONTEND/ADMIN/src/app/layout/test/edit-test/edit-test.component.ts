import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { TestService } from '../../../shared/services/test.service';
import { CommonService } from '../../../shared/services/common.service';
import { TestModel } from '../test-model';
import { InformMessageComponent } from '../../../shared/modal/inform-message/inform-message.component';

declare var CustomWaiting: any;
export interface EditModel {
  testId: string;
}

@Component({
  selector: 'app-edit-test',
  templateUrl: './edit-test.component.html',
  styleUrls: ['./edit-test.component.scss']
})
export class EditTestComponent extends DialogComponent<EditModel, boolean> implements OnInit, EditModel {

  testId: string;
  msgErr_Form: string = "";

  public editTest: TestModel = new TestModel();

  constructor(dialogService: DialogService, public testService: TestService,
    private commonService: CommonService) {
    super(dialogService);
  }

  ngOnInit() {
    this.loadTest();
  }

  loadTest() {
    CustomWaiting.blockUI();
    if (this.testId !== "") {
      this.testService.getTest(this.testId)
        .subscribe(
        response => {
          let res = response['value'];
          if (res.status) {
            this.editTest.TestId = res.data.testId;
            this.editTest.Title = res.data.title;
            this.editTest.Content = res.data.content;
            this.editTest.Description = res.data.description;
          }
          else
            this.dialogService.addDialog(InformMessageComponent, { title: 'Edit Test', message: res.message });
          CustomWaiting.unblockUI();
        },
        error => {
          this.commonService.handleError(error);
          CustomWaiting.unblockUI();
        });
    }
  }

  onSubmit() {
    this.msgErr_Form = "";
    CustomWaiting.blockUI();
    this.testService.editTest(this.editTest)
      .subscribe(response => {
        let res = response['value'];
        if (res.status) {
          this.result = true;
          this.close();
          this.dialogService.addDialog(InformMessageComponent, { title: 'Edit: ' + this.editTest.Title, message: res.message });
        }
        else
          this.dialogService.addDialog(InformMessageComponent, { title: 'Edit: ' + this.editTest.Title, message: res.message });
        CustomWaiting.unblockUI();
      },
      error => {
        this.commonService.handleError(error);
        CustomWaiting.unblockUI();
      })
  }
}
