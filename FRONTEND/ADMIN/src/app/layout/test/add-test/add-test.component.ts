import { Component, OnInit } from '@angular/core';
import { DialogService, DialogComponent } from 'ng2-bootstrap-modal';
import { TestService } from '../../../shared/services/test.service';
import { CommonService } from '../../../shared/services/common.service';
import { TestModel } from '../test-model';
import { InformMessageComponent } from '../../../shared/modal/inform-message/inform-message.component';

declare var CustomWaiting: any;

@Component({
  selector: 'app-add-test',
  templateUrl: './add-test.component.html',
  styleUrls: ['./add-test.component.scss']
})
export class AddTestComponent extends DialogComponent<null, boolean> implements OnInit {

  public test: TestModel = new TestModel();
  public msgErr_Form: string = "";

  constructor(dialogService: DialogService, public testService: TestService,
    private commonService: CommonService) {
    super(dialogService);
  }

  ngOnInit() {
  }

  onSubmit() {
    this.msgErr_Form = "";
    CustomWaiting.blockUI();
    this.testService.addTest(this.test)
      .subscribe(response => {
        let res = response['value'];
        if (res.status) {
          this.result = true;
          this.close();
          this.dialogService.addDialog(InformMessageComponent, { title: 'Add new Test', message: res.message });
        }
        else
          this.msgErr_Form = res.message;
        CustomWaiting.unblockUI();
      },
      error => {
        this.commonService.handleError(error);
        CustomWaiting.unblockUI();
      })
  }
}
