import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestRoutingModule } from './test-routing.module';
import { LoadTestComponent } from './load-test/load-test.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TestDetailComponent } from './test-detail/test-detail.component';
import { AddTestComponent } from './add-test/add-test.component';
import { EditTestComponent } from './edit-test/edit-test.component';
import { DeleteTestComponent } from './delete-test/delete-test.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TestRoutingModule
  ],
  declarations: [LoadTestComponent, TestDetailComponent, AddTestComponent, EditTestComponent, DeleteTestComponent],
  entryComponents: [TestDetailComponent,AddTestComponent, EditTestComponent, DeleteTestComponent]
})
export class TestModule { }
