import { Component, OnInit } from '@angular/core';
import { DialogService } from 'ng2-bootstrap-modal';
import { TestService } from '../../../shared/services/test.service';
import { CommonService } from '../../../shared/services/common.service';
import { TestDetailComponent } from '../test-detail/test-detail.component';
import { AddTestComponent } from '../add-test/add-test.component';
import { EditTestComponent } from '../edit-test/edit-test.component';
import { DeleteTestComponent } from '../delete-test/delete-test.component';
import { InformMessageComponent } from '../../../shared/modal/inform-message/inform-message.component';

declare var CustomWaiting: any;

@Component({
  selector: 'app-load-test',
  templateUrl: './load-test.component.html',
  styleUrls: ['./load-test.component.scss']
})
export class LoadTestComponent implements OnInit {

  tests: any[] = [];

  constructor(private testService: TestService, private dialogService: DialogService,
    private commonService: CommonService) { }

  ngOnInit() {
    this.loadTests();
  }

  loadTests() {
    // CustomWaiting.blockUI();
    this.testService.getTests().subscribe(
      response => {
        let res = response['value'];
        if (res.status) 
          this.tests = res.data;
        else
          this.dialogService.addDialog(InformMessageComponent, { title: 'Load Tests', message: res.message });
        // CustomWaiting.unblockUI();
      },
      error => {
        this.commonService.handleError(error);
        // CustomWaiting.unblockUI();
      });
  }

  detailItem(id) {
    this.dialogService.addDialog(TestDetailComponent, { testId: id });
  }

  addItem() {
    this.dialogService.addDialog(AddTestComponent).subscribe((isSuccess) => {
      if (isSuccess)
        this.loadTests();
    });
  }

  editItem(id) {
    this.dialogService.addDialog(EditTestComponent, { testId: id }).subscribe((isSuccess) => {
      if (isSuccess)
        this.loadTests();
    });
  }

  deleteItem(id, title) {
    this.dialogService.addDialog(DeleteTestComponent, { testId: id, testTitle: title}).subscribe((isSuccess) => {
      if (isSuccess)
        this.loadTests();
    });
  }
}
