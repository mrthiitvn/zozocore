import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { TestService } from '../../../shared/services/test.service';
import { CommonService } from '../../../shared/services/common.service';
import { InformMessageComponent } from '../../../shared/modal/inform-message/inform-message.component';

declare var CustomWaiting: any;
export interface ConfirmModel {
  testId: string;
  testTitle: string;
}

@Component({
  selector: 'app-delete-test',
  templateUrl: './delete-test.component.html',
  styleUrls: ['./delete-test.component.scss']
})
export class DeleteTestComponent extends DialogComponent<ConfirmModel, boolean> implements OnInit, ConfirmModel {

  testId: string;
  testTitle: string;

  constructor(dialogService: DialogService, public testService: TestService,
    private commonService: CommonService) {
    super(dialogService);
  }

  ngOnInit() {
  }

  confirm() {
    if (this.testId !== "") {
      CustomWaiting.blockUI();
      this.testService.deleteTest(this.testId)
        .subscribe(
        response => {
          let res = response['value'];
          if (res.status) {
            this.result = true;
            this.close();
            this.dialogService.addDialog(InformMessageComponent, { title: 'Delete: ' + this.testTitle, message: res.message });
          }
          else
            this.dialogService.addDialog(InformMessageComponent, { title: 'Delete: ' + this.testTitle, message: res.message });
          CustomWaiting.unblockUI();
        },
        error => {
          this.commonService.handleError(error);
          CustomWaiting.unblockUI();
        });
    }
  }

}
