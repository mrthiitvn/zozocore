import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TestService } from '../shared/services/test.service';
import { TestModule } from './test/test.module';

@NgModule({
  imports: [
    CommonModule,
    LayoutRoutingModule,
    TestModule
  ],
  declarations: [LayoutComponent, HeaderComponent, FooterComponent, SidebarComponent],
  providers: [TestService]
})
export class LayoutModule { }
