import { Injectable } from '@angular/core';

@Injectable()
export class CommonService {

  constructor() { }

  /**
   * This is a convenience method for the sake of this example project.
   * Do not use this in production, it's better to handle errors separately.
   * @param error
   */
  handleError(error) {
    console.error('Error processing action', error);
  }
}
