import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from 'ng2-bootstrap-modal';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AppSetting } from '../../app.setting';
import { TestModel } from '../../layout/test/test-model';

@Injectable()
export class TestService {

  constructor(private router: Router, private route: ActivatedRoute, private http: HttpClient,
    private dialogService: DialogService) { }

  setting = new AppSetting();

  // httpOptions = {
  //   headers: new HttpHeaders({
  //     'Content-Type':  'application/json',
  //     'Authorization': 'my-auth-token'
  //   })
  // };

  getTest(id: string) {
    let httpParams = new HttpParams().set("testId", id);
    return this.http.get(this.setting.getTest, { params: httpParams });
  }

  getTests() {
    return this.http.get(this.setting.getTests);
  }

  addTest(test: TestModel) {
    return this.http.post(this.setting.addTest, test);
  }

  editTest(test: TestModel) {
    return this.http.post(this.setting.editTest, test);
  }

  deleteTest(testId: string) {
    let formData = new FormData();
    formData.append('testId', testId);

    return this.http.post(this.setting.deleteTest, formData);
  }
}
