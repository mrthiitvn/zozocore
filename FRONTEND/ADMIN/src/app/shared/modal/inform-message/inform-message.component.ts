import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';

export interface AlertModel {
  title: string;
  message: string;
}

@Component({
  selector: 'app-inform-message',
  templateUrl: './inform-message.component.html',
  styleUrls: ['./inform-message.component.scss']
})
export class InformMessageComponent extends DialogComponent<AlertModel, null> implements OnInit, AlertModel {

  title: string;
  message: string;

  constructor(dialogService: DialogService) { 
    super(dialogService);
  }

  ngOnInit() {
  }

  cancel() {
    this.close();
  }
}
