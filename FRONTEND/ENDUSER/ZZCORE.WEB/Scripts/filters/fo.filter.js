﻿(function (app) {
    'use strict';
    app.filter("jsonDate", function () {
        var re = /\/Date\(([0-9]*)\)\//;
        return function (x) {
            if (x == null) return null;
            var m = x.match(re);
            if (m) return new Date(parseInt(m[1]));
            else return null;
        };
    });
    //SO DIEM DUNG
    //app.filter('filterLightBySDD', filterLightBySDD);
    //function filterLightBySDD() {
    //    return function (FlightChuyenDiModel, SoDiemDung) {
    //        if (FlightChuyenDiModel != null && FlightChuyenDiModel.length > 0) {
    //            var filtered = [];
    //            if ((SoDiemDung != null && SoDiemDung >= 0)) {
    //                for (var i = 0; i < FlightChuyenDiModel.length; i++) {
    //                    var item = FlightChuyenDiModel[i];
    //                    if ((SoDiemDung == 0 || SoDiemDung == 1) && item.SoDiemDung == SoDiemDung)
    //                        filtered.push(item);
    //                    if (SoDiemDung > 1 && item.SoDiemDung > SoDiemDung)
    //                        filtered.push(item);
    //                }
    //            }
    //            return filtered;
    //        } else return FlightChuyenDiModel;
    //    };
    //}
})(angular.module('myapp'));