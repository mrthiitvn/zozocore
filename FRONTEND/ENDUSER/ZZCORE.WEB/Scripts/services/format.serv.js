﻿(function (app) {
    'use strict';
    app.factory('FormatServ', FormatServ);
    function FormatServ() {
        var dateFormatMomentJS = 'DD-MMM-YYYY';
        var dateTimeFormatMomentJS = 'DD-MMM-YYYY HH:mm:ss';
        var services = {
            dateToString: dateToString,
            stringToDate: stringToDate,
            timeToString: timeToString,
            stringToTime: stringToTime,
            addDate: addDate
        };
        function addDate(date, type, value) {
            if (typeof date === 'string') {
                date = stringToDate(date);
            }
            return dateToString(moment(date).add(value, type).toDate());
        }
        function dateToString(date, _format) {
            var format = _format;
            if (format == null) format = dateFormatMomentJS;
            if (typeof date === 'string') {
                date = stringToDate(date);
            }
            return moment(date).format(format);
        }
        function stringToDate(dateString, _format) {
            var format = _format;
            if (format == null) format = dateFormatMomentJS;
            return moment(dateString, format).toDate();
        }
        function timeToString(time, format) {
            if (time == null) return null;
            if (typeof time === 'string') {
                time = stringToDate(time, dateTimeFormatMomentJS);
            }
            return moment(time).format(format || 'HH:mm');
        }
        function stringToTime(timeString) {
            if ((timeString || '') == '') return null;
            if (timeString.length == 5) {
                timeString += ':00';
            }
            return moment(dateToString(new Date()) + ' ' + timeString, dateTimeFormatMomentJS).format(dateTimeFormatMomentJS);
        }
        return services;
    }
})(angular.module('myapp'));