﻿(function (app) {
    'use strict';
    app.factory('MessageServ', MessageServ);
    function MessageServ() {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-bottom-right",//"toast-top-right",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 1000,
            "timeOut": 3000,
            "extendedTimeOut": 1000
        };
        var services = {
            showSuccess: showSuccess,
            showError: showError,
            showWarning: showWarning,
            showInfo: showInfo,
            showConfirm: showConfirm,
            showAlert: showAlert
        };

        function showSuccess(message, timeOut) {
            toastr.options.timeOut = timeOut != null ? timeOut : 3000;
            toastr.success(message);
        }

        function showError(error, timeOut) {
            toastr.options.timeOut = timeOut != null ? timeOut : 3000;
            if (Array.isArray(error)) {
                error.forEach(function (err) {
                    toastr.error(err);
                });
            } else {
                toastr.error(error);
            }
        }

        function showWarning(message, timeOut) {
            toastr.options.timeOut = timeOut != null ? timeOut : 3000;
            toastr.warning(message);
        }

        function showInfo(message, timeOut) {
            toastr.options.timeOut = timeOut != null ? timeOut : 3000;
            toastr.info(message);
        }

        function showConfirm(message, confirmCallBack, cancelCallBack) {
            var confirm = {
                animationSpeed: 300,
                title: message.title,
                content: message.content,
                confirmButton: 'OK',
                confirmButtonClass: 'btn-confirmbox',
                confirm: function () {
                    if (confirmCallBack != null)
                        confirmCallBack();
                },
                cancel: function () {
                    if (cancelCallBack != null)
                        cancelCallBack();
                }
            };
            if (message.columnClass != null) {
                confirm.columnClass = message.columnClass;
            }
            jqConfirm(confirm);
        }

        function showAlert(message, confirmCallBack) {
            var alert = {
                animationSpeed: 300,
                title: message.title,
                content: message.content,
                closeIcon: true,
                confirmButton: 'OK',
                confirmButtonClass: 'btn-confirmbox',
                confirm: function () {
                    if (confirmCallBack != null)
                        confirmCallBack();
                }
            };
            if (message.columnClass != null) {
                alert.columnClass = message.columnClass;
            }
            jqAlert(alert);
        }

        return services;
    }
})(angular.module('myapp'));