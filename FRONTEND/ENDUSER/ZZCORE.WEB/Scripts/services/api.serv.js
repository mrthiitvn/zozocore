﻿(function (app) {
    'use strict';
    app.factory('ApiServ', ApiServ);
    ApiServ.$inject = ['$http', 'MessageServ', '$rootScope', '$location', '$window'];
    function ApiServ($http, MessageServ, $rootScope, $location, $window) {
        var services = {
            get: get,
            post: post,
            del: del,
            upload: upload,
        }

        function get(data) {
            return $http.get(data.url, { params: data.params })
                    .then(function (result) {
                        if (data.success != null) {
                            data.success(result.data);
                        }
                    }, function (error) {
                        if (error.status == '401') {
                            requireSignin();
                        }
                        else if (error.status == '501') {
                            MessageServ.showError('Something went wrong, please refresh webpage and try again', 10000);
                        }
                        else if (data.failure != null) {
                            data.failure(error);
                        }
                    });
        }

        function post(data) {
            return $http.post(data.url, data.params)
                .then(function (result) {
                    if (data.success != null) {
                        data.success(result.data);
                    }
                }, function (error) {
                    if (error.status == '401') {
                        requireSignin();
                    }
                    else if (error.status == '501') {
                        MessageServ.showError('Something went wrong, please refresh webpage and try again', 10000);
                    }
                    else if (data.failure != null) {
                        data.failure(error);
                    }
                });
        }

        function del(data) {
            return $http.delete(data.url, { params: data.params })
                    .then(function (result) {
                        if (data.success != null) {
                            data.success(result.data);
                        }
                    }, function (error) {
                        if (error.status == '401') {
                            requireSignin();
                        }
                        else if (error.status == '501') {
                            MessageServ.showError('Something went wrong, please refresh webpage and try again', 10000);
                        }
                        else if (data.failure != null) {
                            data.failure(error);
                        }
                    });
        }

        function upload(data) {
            if (data.files != null && data.files.length > 0) {
                var fd = new FormData();
                for (var i = 0; i < data.files.length; i++) {
                   fd.append("file", data.files[i]);
                }
                return $http.post(data.url, fd, {
                    withCredentials: true,
                    headers: { 'Content-Type': undefined },
                    transformRequest: angular.identity
                }).then(function (result) {
                    if (data.success != null) {
                        data.success(result.data);
                    }
                }, function (error) {
                    if (error.status == '401') {
                        requireSignin();
                    }
                    else if (error.status == '501') {
                        MessageServ.showError('Something went wrong, please refresh webpage and try again', 10000);
                    }
                    else if (data.failure != null) {
                        data.failure(error);
                    }
                });
            }
        }

        function requireSignin() {
            var repo = $rootScope.repository || {};
            if (repo.user != null) {
                MessageServ.showWarning('You do not have permission to perform this function !', 10000);
            }
            else {
                MessageServ.showWarning('You must login before do this action', 10000);
            }
            if (repo.signin != null) {
                repo.signin();
            }
        }
        return services;

    }
})(angular.module('myapp'))