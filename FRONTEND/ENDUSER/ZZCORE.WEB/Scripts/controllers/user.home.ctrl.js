﻿(function (app) {
    'use strict';
    app.controller('UserHomeCtrl', function (MessageServ, ApiServ, $scope, $sessionStorage) {
        $scope.load = function () {
            $scope.loadInit();
            $scope.getBanners();
        }
        $scope.loadInit = function () {
            alert("gia tri session da luu: " + $sessionStorage.vidu);
        }
        $scope.getBanners = function () {
            ApiServ.get({
                url: '/home/GetBanners',
                //params: { PaymentMethod: typeMethod, OrderID: $sessionStorage.OrderID },
                success: function (result) {
                    $scope.models = result;
                },
                failure: function (error) {
                    MessageServ.showError('Lấy banners không thành công !');
                }
            });
        }
        $scope.postDataToServer = function () {
            if (checkValidate($scope.model)) {
                ApiServ.post({
                    url: '/home/SavePaymentMethod',
                    params: { PaymentMethod: typeMethod, OrderID: $sessionStorage.OrderID },
                    success: function (result) {
                        MessageServ.showSuccess('Luu ok');
                    },
                    failure: function (error) {
                        MessageServ.showError('Lỗi 500: Lưu phương thức thanh toán bị lỗi !');
                    }
                });
            }
        }
        function checkValidate(model) {
            if (model.name == null || model.name == "") {
                MessageServ.showError('Vui long ktra lai thong tin NAME!');
                return false;
            }
            if (model.email == null || model.email == "") {
                MessageServ.showError('Vui long ktra lai thong tin email!');
                return false;
            }
            return true;
        }
        $scope.load();
    });
})(angular.module('myapp'));