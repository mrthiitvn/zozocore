﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.WebPages;

namespace ZZCORE.WEB
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //adding a display modes for mobile device
            //DisplayModeProvider.Instance.Modes.Insert(0,
            //    new DefaultDisplayMode("wp75")
            //    {
            //        ContextCondition = (context => (context.Request.UserAgent.IndexOf("Windows Phone OS 7.5", StringComparison.OrdinalIgnoreCase)
            //            >= 0))
            //    });
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
