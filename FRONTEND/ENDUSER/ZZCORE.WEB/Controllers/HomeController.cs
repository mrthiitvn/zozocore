﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ZZCORE.WEB.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GuestHome()
        {
            return View();
        }
        public ActionResult UserHome()
        {
            return View();
        }
        public ActionResult UserPrivate()
        {
            return View();
        }
        public ActionResult UserShops()
        {
            return View();
        }
        public ActionResult UserFriends()
        {
            return View();
        }
        public ActionResult UserGuestView()
        {
            return View();
        }
        public ActionResult UserBuying()
        {
            return View();
        }
        public ActionResult UserEarning()
        {
            return View();
        }
        public ActionResult PagePrivate()
        {
            return View();
        }
        public ActionResult SearchResultPeople()
        {
            return View();
        }
        public ActionResult SearchResultPosts()
        {
            return View();
        }
        public ActionResult SearchResultShops()
        {
            return View();
        }
        public ActionResult SearchResultProducts()
        {
            return View();
        }
        public ActionResult PageGuestView()
        {
            return View();
        }
        public ActionResult ProductDetails()
        {
            return View();
        }
        public ActionResult Checkout()
        {
            return View();
        }
        public ActionResult Success()
        {
            return View();
        }
        public ActionResult GuestCategories()
        {
            return View();
        }
        public ActionResult Chat()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Blank()
        {
            return View();
        }
    }
}