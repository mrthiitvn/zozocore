﻿$(function () {
    
    $('input, textarea').on('change', function () {
        $(this).attr('value', $(this).val());
    });
    $('input, textarea').focus(function () {
        $(this).parent().addClass('active');
    }).blur(function () {
        $(this).parent().removeClass('active');
        });
    //init tabs
    tabby.init({
        selectorToggle: '.tab', // Tab toggle selector
        selectorToggleGroup: '.tabs', // Tab toggle group selector
        selectorContent: '.tab-content', // Tab content selector
        selectorContentGroup: '.tabs-container', // Tab content group selector
        toggleActiveClass: 'active', // Class added to active toggle elements
        contentActiveClass: 'active', // Class added to active tab content areas
        initClass: 'js-tabby', // Class added to <html> element when initiated
        stopVideo: true, // [Boolean] If true, stop videos when tab closes
        callback: function (tabs, toggle) { } // Function that's run after tab content is toggled
    });
    //restyle select boxes
    $('select.sumoselect').SumoSelect();
    //slicks
    $('#sanPhamPhoBien').slick({
        infinite: false,
        slidesToShow: 5,
        slidesToScroll: 1,
    });
    $('#sanPhamPhoBien_buying').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
    });
    $('#megaSale').slick({
        infinite: false,
        slidesToShow: 5,
        slidesToScroll: 1,
    });
    $('#megaSale_buying').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
    });
    $('#gianHangChinhHang').slick({
        infinite: false,
        slidesToShow: 5,
        slidesToScroll: 1,
    });
    $('#gianHangChinhHang_buying').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
    });
    $('#danhMucNganhHang').slick({
        infinite: false,
        slidesToShow: 5,
        slidesToScroll: 1,
    });
    $('#danhMucNganhHang_buying').slick({
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 1,
    });
    $('#sanPhamGoiY').slick({
        infinite: false,
        slidesToShow: 5,
        slidesToScroll: 1,
    });
    $('#sanPhamGoiY_buying').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
    });
    //dropdown
    $('.dropdown>.label').click(function (event) {
        event.stopPropagation();
        var parent = $(this).parent();
        var content = $(this).siblings('.content');
        parent.toggleClass('show');
        parent.click(function (event) {
            event.stopPropagation();
        });
        $(window).click(function () { parent.removeClass('show'); });
        $('.dropdown').not(parent).removeClass('show');
    });
    //number spinner
    $("input[type='number']").InputSpinner();
    //User left menu fixed to parent
    $("#user-home-left-fixed").stick_in_parent({ offset_top: 53 });
    //user right menu size fix
    var friendListVerticleHeight = $(window).innerHeight() - 53;
    $('#friend-list-verticle').css({ height: friendListVerticleHeight + 'px' });
    setTimeout(function () {
        $("#user-home-right-fixed").stick_in_parent({ offset_top: 53 });
    }, 100);
    $(window).resize(function () {
        var friendListVerticleHeight = $(window).innerHeight() - 53;
        $('#friend-list-verticle').css({ height: friendListVerticleHeight + 'px' });
        setTimeout(function () {
            $("#user-home-right-fixed").stick_in_parent({ offset_top: 53 });
        }, 100);
    });
    //cuộn breadcrumb trang mua sắm
    var breadcrumbsBuying = $('#breadcrumbs-buying');
    if (breadcrumbsBuying != null && breadcrumbsBuying != undefined) {
        var breadcrumbsBuyingWidth = breadcrumbsBuying.width();
        var breadcrumbsBuyingParentWidth = breadcrumbsBuying.parent('.breadcrumbs-slide').width();
        var breadcrumbsScrollLeft = breadcrumbsBuying.parent().find('.scroll-left').eq(0);
        var breadcrumbsScrollRight = breadcrumbsBuying.parent().find('.scroll-right').eq(0);
        var breadcrumbsBuyingDraggie = $('#breadcrumbs-buying').draggabilly({
            axis: 'x'
        });
        var rePositionBreadCrumb = function () {
            if (breadcrumbsBuying.position().left < 0 && breadcrumbsBuying.position().left > -(breadcrumbsBuyingWidth - breadcrumbsBuyingParentWidth)) {
                breadcrumbsScrollLeft.css({ visibility: 'visible', opacity: 1 });
                breadcrumbsScrollRight.css({ visibility: 'visible', opacity: 1 });
            }
            else if (breadcrumbsBuying.position().left >= 0) {
                breadcrumbsBuying.animate({ left: 0 }, 300);
                breadcrumbsScrollLeft.css({ visibility: 'hidden', opacity: 0 });
            } else if (breadcrumbsBuying.position().left <= -(breadcrumbsBuyingWidth - breadcrumbsBuyingParentWidth)) {
                breadcrumbsBuying.animate({ left: -(breadcrumbsBuyingWidth - breadcrumbsBuyingParentWidth) }, 300);
                breadcrumbsScrollRight.css({ visibility: 'hidden', opacity: 0 });
            }
        }
        breadcrumbsBuyingDraggie.on('dragEnd', function (event, pointer) {
            rePositionBreadCrumb();
        });
        breadcrumbsScrollLeft.click(function () {
            console.log('scroll-left');
            breadcrumbsBuying.animate({ left: Math.min(breadcrumbsBuying.position().left + 300, 0) }, 300, rePositionBreadCrumb);
        });
        breadcrumbsScrollRight.click(function () {
            console.log('scroll-right');
            breadcrumbsBuying.animate({ left: Math.max(breadcrumbsBuying.position().left - 300, -(breadcrumbsBuyingWidth - breadcrumbsBuyingParentWidth)) }, 300, rePositionBreadCrumb);
        });
    }
    //Hết mục cuộn breadcrumb trang mua sắm
    //Product image gallery
    //$('.xzoom, .xzoom-gallery').xzoom({});
    //Easyzoom
    // Instantiate EasyZoom instances
    var $easyzoom = $('.easyzoom').easyZoom();

    // Setup thumbnails example
    var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');

    $('.thumbnails').on('click', 'a', function (e) {
        var $this = $(this);

        e.preventDefault();

        // Use EasyZoom's `swap` method
        api1.swap($this.data('standard'), $this.attr('href'));
    });
    //Hết Easyzoom
    //popup

    //hiện body ra sau cùng
    $('body').css({ visibility: 'visible', opacity: 1 });
});